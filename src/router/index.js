
import { createRouter, createWebHistory } from "vue-router";
import Setup from "../views/Setup.vue";
import Game from "../views/Game.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Setup",
      component: Setup,
      
    },
    {
      path: "/Game",
      name: "Game",
      component: Game,
      props: true,
    },
    /* {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    }, */
  ],
});

export default router;
